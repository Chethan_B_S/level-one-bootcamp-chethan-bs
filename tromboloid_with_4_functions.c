//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
int input()
{
    float a; 
    scanf("%f",&a);
    return a;
}

float volume(float h,float d,float b)
{
    float vol;
    vol=(1.0/3*((h*d)+d))/b;
    return vol;
}

void output(float res)
{
    printf("Volume of a tromboloid: %f",res);
}

int main()
{
    float h,d,b,res;
    printf("Enter the value of h: ");
    h=input();
    printf("Enter the value of d: ");
    d=input();
    printf("Enter the value of b: ");
    b=input();
    res=volume(h,d,b);
    output(res);
    return 0;
}