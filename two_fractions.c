#include<stdio.h>
struct fract
{
	int x;
	int y;
};
typedef struct fract fract;
fract input(int n)
{
	fract x;
	printf("Enter the value of numerator for fract %d :\n",n);
	scanf("%d",&x.x);
	printf("Enter the value of denominator for fract %d :\n",n);
	scanf("%d",&x.y);
	return x;
}
int find_gcd(int a, int b)
{
    int temp;
	while (a!=0)
	{
	    temp=a;
	    a=b%a;
	    b=temp;
	}
	return b;
}
fract simplify(fract sum)
{
	int gcd=find_gcd(sum.x,sum.y);
	sum.x=sum.x/gcd;
	sum.y=sum.y/gcd;
	return sum;
}
fract compute_sum(fract f1, fract f2)
{
    fract sum;
    sum.x=(f1.x*f2.y)+(f2.x*f1.y);
    sum.y=f1.y*f2.y;
	sum=simplify(sum);
    return sum;
}
void display_sum(fract f1, fract f2, fract sum)
{
	printf("The sum of %d/%d and %d/%d is: %d/%d.\n",f1.x,f1.y,f2.x,f2.y,sum.x,sum.y);
}
int main()
{
	fract f1,f2,sum;
	f1=input(1);
	f2=input(2);
	sum=compute_sum(f1,f2);
	display_sum(f1,f2,sum);
	return 0;
}


