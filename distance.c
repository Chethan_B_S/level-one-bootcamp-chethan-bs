//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include<math.h>
float input()
{
    float a; 
    scanf("%f",&a);
    return a;
}

float distance(float x1,float y1,float x2,float y2 )
{
    float distance;
    distance=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    return distance;
}

void output(float res)
{
    printf("distance between two points: %f",res);
}

int main()
{
    float x1,y1,x2,y2,res;
    printf("Enter the value of x1: ");
    x1=input();
    printf("Enter the value of x2: ");
    x2=input();
    printf("Enter the value of y1: ");
    y1=input();
    printf("Enter the value of y2: ");
    y2=input();
    res=distance(x1,y1,x2,y2);
    output(res);
    return 0;
}
